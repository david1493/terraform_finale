variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "nom_resource" {}
variable "network_name" {}
variable "subnet" {}
variable "publicIP" {}
variable "NetworkSecurityGroup" {}
variable "NetworkInterface" {}
variable "NicConfiguration" {}
variable "name_virtual_machine" {}
variable "security_rule" {}
variable "storage_disk" {}
variable "location_world" {}
variable "environment_name" {}
variable "public_ip_address_allocation_name" {}
variable "address_space_number" {}
variable "ip_addresses"  {type = "list"}
